from techAcademyController import TechAcademyController

def testname():

    techAcademyController = TechAcademyController()
    techAcademyController.sendInput("r1Input", 'rock')
    techAcademyController.click("r1Btn")
    banner = techAcademyController.getTextbyId("passwordBanner")
    techAcademyController.sendInput("r2Input", banner)
    merchant1 = techAcademyController.getMerchantInfo('//*[@id="block-05ea3afedc551e378bdc"]/div/div[3]/span/b', '//*[@id="block-05ea3afedc551e378bdc"]/div/div[3]/p')
    merchant2 = techAcademyController.getMerchantInfo('//*[@id="block-05ea3afedc551e378bdc"]/div/div[4]/span/b', '//*[@id="block-05ea3afedc551e378bdc"]/div/div[4]/p')

    if (merchant1.get("wealtch") > merchant2.get("wealtch")):
        techAcademyController.sendInput("r3Input", merchant1.get("name"))
    else:
        techAcademyController.sendInput("r3Input", merchant2.get("name"))

    techAcademyController.click("r3Butn")
    lastbanner = techAcademyController.getTextbyXpath('//*[@id="successBanner2"]/h4')

    techAcademyController.waitClick("checkButn")

    assert lastbanner == "Success!"
