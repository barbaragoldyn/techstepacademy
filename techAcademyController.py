from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TechAcademyController:

    def __init__(self):
        self.driver = webdriver.Chrome( ChromeDriverManager().install() )
        self.driver.get("https://techstepacademy.com/trial-of-the-stones")

    def sendInput(self, inputId, value):
        self.driver.find_element(By.ID, inputId).send_keys(value)

    def click(self, buttonId):
        self.driver.find_element(By.ID, buttonId).click()

    def waitClick(self, buttonId):
        element = WebDriverWait(self.driver, 4).until(
            EC.element_to_be_clickable( (By.ID, buttonId)))
        element.click()

    def getTextbyId(self, bannerId):
        return self.driver.find_element(By.ID, bannerId).text

    def getTextbyXpath(self, bannerXpath):
        return self.driver.find_element(By.XPATH, bannerXpath).text

    def getMerchantInfo(self, nameXpath, wealtchXpath):
        name = self.driver.find_element(By.XPATH, nameXpath).text
        wealtch = int(self.driver.find_element(By.XPATH, wealtchXpath).text)
        return {
            "name": name,
            "wealtch": wealtch
        }